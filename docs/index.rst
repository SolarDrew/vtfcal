Documentation
=============

This is the documentation for VTFcal.
Calibration tools for removing instrumental and atmospheric effects from data collected by the Visible Tunable Filter on the Daniel K. Inouye Solar Telescope

.. toctree::
  :maxdepth: 2

  vtfcal/index.rst

.. note:: The layout of this directory is simply a suggestion.  To follow
          traditional practice, do *not* edit this page, but instead place
          all documentation for the package inside ``vtfcal/``.
          You can follow this practice or choose your own layout.
