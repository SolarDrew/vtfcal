Calibration pipeline for the DKIST/VTF
--------------------------------------

.. image:: http://img.shields.io/badge/powered%20by-SunPy-orange.svg?style=flat 
    :target: http://www.sunpy.org                                               
    :alt: Powered by SunPy Badge    

Calibration tools for removing instrumental and atmospheric effects from data collected by the Visible Tunable Filter on the Daniel K. Inouye Solar Telescope


License
-------

This project is Copyright (c) Aperio Software Ltd. and licensed under the terms of the BSD 3-Clause license. See the licenses folder for more information.
