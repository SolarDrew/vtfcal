"""
Short summary

Extended summary

See Also
--------
Optional

Notes
-----
Optional

References
----------
Optional, use if references are cited in Notes

Examples
--------
Optional
"""

import logging
from itertools import groupby

import numpy as np

import ccdproc

from vtfcal.utils import get_wavelength_step


## This and other new functions in this repo should take and return an asdf tree for consistency
## with existing ones.
def flat_correct(data, flats):
    logger = logging.getLogger(__name__)
    logger.setLevel("INFO")

    corrected_frames = []
    ## OPTIMISE: remove loop
    wlsteps = groupby([f.fileuri for f in data], key=get_wavelength_step)
    for l, frames in wlsteps:
        for fileobj in frames:
            ## REFACTOR: file loading
            dataframe = ccdproc.CCDData.read(fileobj, format="fits", unit="adu")
            newpath = fileobj.replace("dark_corrected_", "flat_corrected_")
            if isinstance(flats, list):
                flat = flats[l - 1] if len(flats) > 1 else flats[0]
            elif isinstance(flats, ccdproc.ccddata.CCDData):
                flat = flats
            logger.debug(f"{np.nanmin(flat.data)}")
            dataframe = ccdproc.flat_correct(dataframe, flat, norm_value=1)
            dataframe.write(newpath)
            corrected_frames.append(newpath)

    return corrected_frames
