import logging
from pathlib import Path

import numpy as np

import asdf
from astropy.io import fits

from dkist.asdf_maker import headers_from_filenames as heads
from dkist.asdf_maker import references_from_filenames as refs
from dkist.io import DaskFITSArrayContainer as DFAC
from dkist.io.fits import AstropyFITSLoader as Loader
from vtfcal import utils
from vtfcal.utils import get_aquisition, get_wavelength_step, plotprofile


def reduce_prefilter(data_tree):
    """
    """

    logger = logging.getLogger(__name__)

    asdf_file = asdf.open(data_tree, mode="rw")
    outdir = asdf_file["support"]["data_dir"]

    # prefilter_frames = sorted([f.fileuri for f in asdf_file["raw"]["prefilter modstate0"]], key=utils.get_starttime)
    asdf_file = utils.average_by_wavelength(asdf_file, "prefilter")

    modstates = utils.modstates
    for modstate in modstates:
        prefilter_files = asdf_file["support"][f"reduced averaged prefilter {modstate}"]
        prefilter_fnames = sorted([f.fileuri for f in prefilter_files], key=get_wavelength_step)
        prefilter_frames = DFAC(prefilter_files, loader=Loader).array

        norm_frames = (prefilter_frames / np.nanmax(prefilter_frames, axis=0)).compute()
        norm_fnames = []
        for wl, frame in enumerate(norm_frames):
            header = fits.getheader(prefilter_fnames[wl])
            fname = Path(outdir) / modstate / f"normalised_prefilter_l{wl:02}a0.FITS"
            fits.writeto(fname, frame, header=header, overwrite=True)
            norm_fnames.append(fname)
        asdf_file["support"][f"reduced normalised prefilter {modstate}"] = refs(
            norm_fnames, np.array(heads(norm_fnames)), len(norm_fnames)
        )

    asdf_file.update()

    for modstate in modstates:
        plotprofile(
            asdf_file,
            [(f"prefilter {modstate}", "Raw prefilter"),],
            f"00a-prefilter-{modstate}",
            linestyle=[None],
            color=["black"],
            raw=True,
        )
        plotprofile(
            asdf_file,
            [
                (f"reduced averaged prefilter {modstate}", "Averaged prefilter"),
                (f"reduced normalised prefilter {modstate}", "Normalised prefilter"),
            ],
            f"00b-prefilter-{modstate}",
            linestyle=[None, "--"],
            color=["red", "blue"],
        )
