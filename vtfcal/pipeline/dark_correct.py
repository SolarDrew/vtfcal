"""
Science data preparation

Functions to perform dark corrections on raw science data as part of the preparation for more
rigourous image reconstruction using those data.

See Also
--------
Optional

Notes
-----
Optional

References
----------
Optional, use if references are cited in Notes

Examples
--------
Optional
"""

import logging

import asdf

from vtfcal import utils
from vtfcal.test_constants import TEST_WL_IDX


def correct_darks(data_tree):
    """
    Apply dark correction to raw data frames.

    Loads data frames from the input directory specified by `data_tree` and corrects them for dark
    effects using the average dark calculated using :meth:`commands.reduce_darks`. Corrected frames
    are saved to the output directory specified by `data_tree` and references to the files are added
    to the tree for use later in the calibration process.

    \b
    Parameters
    ----------
    data_tree : string or :class:`pathlib.Path`
        Path to an :class:`~asdf.AsdfFile` defining the calibration data structure, including input
        and output data directories, and file references to averaged darks. See
        :meth:`commands.init_data_tree` for generating an appropriate file.

    Examples
    --------

    """
    logger = logging.getLogger(__name__)
    logger.setLevel("INFO")

    asdf_file = asdf.open(data_tree, mode="rw")
    # iterate over groups
    modstates = utils.modstates
    for modstate in modstates:
        asdf_file = utils.correct_darks(asdf_file, "raw", f"data {modstate}")
        logger.info(f"Demo plots created with wavelength index {TEST_WL_IDX}")
        logger.debug(asdf_file["support"][f"corrected dark-corrected data {modstate}"])

        utils.plotframes(
            asdf_file,
            [(f"corrected dark-corrected data {modstate}", "Dark-corrected data frame",)],
            "02a-dark-corrected-data",
        )
