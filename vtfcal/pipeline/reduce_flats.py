"""
Flat frame reduction

Average collected flat frames as appropriate to reduce to the smallest number required to calibrate
the science data.

See Also
--------
Optional

Notes
-----
Optional

References
----------
Optional, use if references are cited in Notes

Examples
--------
Optional
"""

import logging
from os.path import join
from pathlib import Path
from itertools import groupby
from multiprocessing.pool import ThreadPool

import dask
import dask.array as da
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np
from dask.distributed import Client

import asdf
from astropy.io import fits

from dkist.asdf_maker import headers_from_filenames as heads
from dkist.asdf_maker import references_from_filenames as refs
from dkist.io import DaskFITSArrayContainer as DFAC
from dkist.io.fits import AstropyFITSLoader as Loader
from vtfcal.reduction.flats import average_flats, calculate_wl_shift, correct_wl_shift
from vtfcal.test_constants import TEST_PIXEL, TEST_WL_IDX
from vtfcal.utils import (
    correct_darks,
    get_dlambda,
    get_scanrange,
    get_starttime,
    get_wavelength,
    modstates,
    plotframes,
    plotprofile,
)

# Set up dask for threading of intensive tasks.
# Need this here because Client doesn't like being instantiated not in __main__.
if __name__ == "__main__":
    client = Client()
    dask.config.set(pool=ThreadPool())


def reduce_flats(
    data_tree,
    input_flats_key="reduced averaged flats",
    correction=True,
    fourier=True,
    modstates=modstates,
):
    """
    Function to reduce VTF flat frames

    Loads flat frames from the input directory specified by `data_tree` and reduces them by applying
    the following steps:

    - Group frames by wavelength position and average them
    - Correct for darks
    - Calculate and correct for wavelength shift
    - Normalise images by scaling each frame by its average value [still needs implementing]

    Reduced flats are saved to the output directory specified by `data_tree` and references to the
    files are added to the tree for use later in the calibration process.

    Parameters
    ----------
    data_tree : string or :class:`pathlib.Path`
        Path to an :class:`~asdf.AsdfFile` defining the calibration data structure, including input
        and output data directories, and file references to averaged darks. See
        :meth:`commands.init_data_tree` for generating an appropriate file.

    Examples
    --------

    """
    logger = logging.getLogger(__name__)
    logger.setLevel("INFO")

    asdf_file = asdf.open(data_tree, mode="rw")
    if asdf_file["mode"] not in ["broadband", "narrowband"]:
        raise ValueError(
            f'Unrecognised data mode in asdf tree: {asdf_file["mode"]} '
            '- "mode" keyword should be either "broadband" or "narrowband".'
        )

    outdir = asdf_file["support"]["data_dir"]

    asdf_file = average_flats(asdf_file)
    if asdf_file["mode"] == "broadband":
        modstates = [""]
    for modstate in modstates:
        asdf_file = correct_darks(asdf_file, "support", f"{input_flats_key} {modstate}".rstrip())
        logger.debug(f"{asdf_file['mode']} - {input_flats_key} {modstate}")
        logger.debug(f"{asdf_file['support'][f'{input_flats_key} {modstate}'.rstrip()]}")
        dark_corrected_files = asdf_file["support"][
            f"corrected dark-corrected flats {modstate}".rstrip()
        ]
        logger.debug(dark_corrected_files)

        if asdf_file["mode"] == "broadband":
            continue

        dark_corrected_flats = DFAC(dark_corrected_files, loader=Loader).array

        plotframes(
            asdf_file,
            [
                (f"{input_flats_key} {modstate}", "Averaged flat frames"),
                (f"reduced averaged darks", "Averaged dark frame"),
                (f"corrected dark-corrected flats {modstate}", "Dark-corrected flat frame",),
            ],
            "01aii-flat-correction",
        )

        plotprofile(
            asdf_file,
            [
                (f"{input_flats_key} {modstate}", "Raw flats"),
                (f"corrected dark-corrected flats {modstate}", "Dark-corrected flats"),
            ],
            f"01bii-averaged-vs-corrected-profiles-{modstate}",
            linestyle=[":", "--"],
            color=["red", "blue"],
        )

        if not correction:
            norm_frames = (
                dark_corrected_flats
                / np.nanmean(dark_corrected_flats, axis=(1, 2))[:, np.newaxis, np.newaxis]
            )
            norm_fnames = []
            for wl, frame in enumerate(norm_frames):
                fname = Path(outdir) / modstate / f"normalised_flat_l{wl:02}a0.FITS"
                fits.writeto(fname, frame.compute(), overwrite=True)
                norm_fnames.append(fname)
            asdf_file["support"]["corrected normalised flats " + modstate] = refs(
                norm_fnames, np.array(heads(norm_fnames)), len(norm_fnames)
            )
            asdf_file.update()

            mean_profile = np.nanmean(dark_corrected_flats, axis=(1, 2))
            plotprofile(
                asdf_file,
                [
                    (f"{input_flats_key} {modstate}", "Raw flats"),
                    (f"corrected dark-corrected flats {modstate}", "Dark-corrected flats",),
                    (mean_profile.reshape(len(norm_frames)), "Average profile"),
                    (f"corrected normalised flats {modstate}", "Normalised flats"),
                ],
                f"profile-comparison-{modstate}",
                linestyle=["-.", ":", "--", None],
                color=["black", "red", "blue", "green"],
            )

            continue

        asdf_file, dark_corrected_flats = calculate_wl_shift(asdf_file, modstate, fourier=fourier)

        meanflat_files = sorted(
            sorted(asdf_file["support"][f"{input_flats_key} {modstate}"], key=get_wavelength),
            key=get_starttime,
        )
        pf_corrected_flats = [frame for frame in DFAC(meanflat_files, loader=Loader).array]
        for start, files in groupby(meanflat_files, key=get_starttime):
            files = list(files)
            logger.debug([get_wavelength(f) for f in files])
            meanflat_frames = DFAC(files, loader=Loader).array

            # get prefilter frames
            prefilter_files = sorted(
                asdf_file["support"][f"reduced averaged prefilter {modstate}"], key=get_wavelength
            )
            # crop prefilter scan to flats scan start/stop values
            scan_start, scan_stop = get_scanrange(files[0])
            logger.debug(f"{scan_start}, {scan_stop}")
            prefilter_files = [
                f for f in prefilter_files if scan_start <= get_wavelength(f) <= scan_stop
            ]
            prefilter_frames = DFAC(prefilter_files, loader=Loader).array

            n_positions = len(files)
            dlam = get_dlambda(files[0])
            prefilter_files = sorted(prefilter_files, key=get_starttime)
            for pf_starttime, pfscan in groupby(prefilter_files, key=get_starttime):
                pfscan = list(pfscan)
                pf_scan_start, pf_scan_stop = get_scanrange(pfscan[0])
                pf_waves = sorted([get_wavelength(f) for f in pfscan])
                if (
                    len(pfscan) >= n_positions and pf_scan_start <= scan_start
                ):  # and get_dlambda(pfscan[0]) == dlam):
                    if (scan_start + n_positions * dlam) <= pf_scan_stop:
                        idx = pf_waves.index(scan_start)
                        pfcurve = prefilter_frames[idx : idx + n_positions]
                        # check if scan axis is identical and then perform correction step
                        for i, flatfile in enumerate(files):
                            logger.debug(f"{i}, {flatfile}")
                            flatidx = meanflat_files.index(flatfile)
                            pf_corrected_flats[flatidx] = meanflat_frames[i] / pfcurve[i]

        pf_corrected_flats = da.stack(pf_corrected_flats)
        logger.debug(pf_corrected_flats)

        # TODO This needs to run individually for each scan as well
        wl_shift_map = fits.open(
            asdf_file["support"]["calibration wl-shift-map " + modstate][0].fileuri
        )[0].data
        shifted_flats = correct_wl_shift(
            pf_corrected_flats, wl_shift_map, fourier=fourier,
        ).compute()

        shifted_files = []
        old_headers = heads([f.fileuri for f in dark_corrected_files])
        copy_keys = sorted([k for k in old_headers[0].keys() if "VTF" in k or "DKIST" in k])
        # TODO This really needs some optimisation
        for wl, frame in enumerate(shifted_flats):
            filename = Path(outdir) / modstate / f"wl_shifted_flat_l{wl:02}a0.FITS"
            head = old_headers[wl]
            new_head = fits.Header({kw: head[kw] for kw in copy_keys})
            logger.debug(filename)
            fits.writeto(filename, frame, new_head, overwrite=True)
            shifted_files.append(filename)
        asdf_file["support"]["corrected wl-shifted flats " + modstate] = refs(
            shifted_files, np.array(heads(shifted_files)), len(shifted_files)
        )

        plotframes(
            asdf_file,
            [
                (f"corrected dark-corrected flats {modstate}", "Dark-corrected flat frame",),
                (f"calibration wl-shift-map {modstate}", r"$\lambda$-shift map"),
                (f"corrected wl-shifted flats {modstate}", r"$\lambda$-shifted flat frame",),
            ],
            f"01g-wl-shift-comparison-{modstate}",
        )

        logger.debug(shifted_flats.shape)

        mean_shifted_profile = da.nanmean(shifted_flats, axis=(1, 2))
        unshifted_mean = correct_wl_shift(
            mean_shifted_profile[:, np.newaxis, np.newaxis], -wl_shift_map, fourier=fourier,
        )
        norm_frames = (dark_corrected_flats / unshifted_mean).compute()
        norm_fnames = []
        for wl, frame in enumerate(norm_frames):
            fname = Path(outdir) / modstate / f"normalised_flat_l{wl:02}a0.FITS"
            head = old_headers[wl]
            new_head = fits.Header({kw: head[kw] for kw in copy_keys})
            fits.writeto(fname, frame, new_head, overwrite=True)
            norm_fnames.append(fname)
        asdf_file["support"]["corrected normalised flats " + modstate] = refs(
            norm_fnames, np.array(heads(norm_fnames)), len(norm_fnames)
        )
        asdf_file.update()

        plotprofile(
            asdf_file,
            [
                (f"corrected wl-shifted flats {modstate}", r"Mean $\lambda$-shifted profile",),
                (f"corrected dark-corrected flats {modstate}", "Dark-corrected profile",),
                (f"corrected normalised flats {modstate}", "Normalised flats"),
            ],
            f"01i-normalisation-comparison-{modstate}",
            plot_mean=[True, False, False],
            linestyle=[":", "--", None],
            color=["black", "blue", "green"],
        )
