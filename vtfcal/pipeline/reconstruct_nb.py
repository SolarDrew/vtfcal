"""
Image reconstruction


See Also
--------
Optional

Notes
-----
Optional

References
----------
Optional, use if references are cited in Notes

Examples
--------
Optional
"""

import logging
import configparser as cfp
from os.path import join
from pathlib import Path

import dask.array as da
import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import fft2, fftshift, ifft2, ifftshift

import asdf
from astropy.io import fits

from dkist.asdf_maker import headers_from_filenames as heads
from dkist.asdf_maker import references_from_filenames as refs
from dkist.io import DaskFITSArrayContainer as DFAC
from dkist.io.fits import AstropyFITSLoader as Loader
from vtfcal.test_constants import TEST_WL_IDX
from vtfcal.utils import correct_darks, plotframes


def apod_filter(image, width):
    x, y = image.shape[-2:]
    if x == 1 or y == 1:
        return image
    r = width * 2
    dom = np.arange(2 * r)
    ran = 0.5 * (1 - np.cos(np.pi * dom / r))
    xx, yy = np.ones((x, y)), np.ones((x, y))
    xx[:r, :] = ran[:r, np.newaxis]
    xx[-r:, :] = ran[r:, np.newaxis]
    yy[:, :r] = ran[:r]
    yy[:, -r:] = ran[r:]
    apod = xx * yy

    return image * apod


def fft_chunks(image):
    # return fftshift(fft2(apod_filter(image, 8)))
    return fftshift(fft2(image))


def calc_noise(fft_bb, fsum, bsum):

    noise = fft_bb * (fsum / bsum)

    return noise


def calc_im(fft_bb, nsum, bsum):
    # Really not sure if this should be fft_bb or fftspeck_bb, but I'm doing this for now to make it
    # actually run
    # return fftspeck_bb * (nsum / bsum)
    return fft_bb * (nsum / bsum)


def calc_power(recon_im):
    # Summing actually breaks the algorithm and I need to figure out why and what to do about it
    return np.abs(recon_im) ** 2  # .sum()


def calc_optfilter(recon_noise, data_power):
    opt_filter = (data_power - recon_noise) / data_power
    # Shift interesting parts of the spectrum to the centre
    # opt_filter = fftshift(opt_filter)
    # smooth optimum_filter

    return opt_filter


def reconstruct_image(recon_im, opt_filter, overlap):
    filtered_im = recon_im * opt_filter
    recon_nb = ifft2(ifftshift(filtered_im))
    # recon_nb = ifft2(filtered_im)

    return apod_filter(recon_nb, overlap)


def reconstruct(data_tree, bb_tree=None):
    """
    Function to reconstruct VTF science data

    Loads science frames from the input directory specified by `data_tree` and reconstructs them by
    applying the process described in the VTF algorithm document [1]_.

    Parameters
    ----------
    data_trees : string or :class:`pathlib.Path`
        Paths to :class:`~asdf.AsdfFile`s defining the calibration data structure, including input and output
        data directories, and file references to prepared science data. See
        :meth:`commands.init_data_tree` for generating appropriate files.

    Examples
    --------

    References
    ----------
    .. _[1] VTF/DKIST data pipeline - The algorithm structure
    """

    """
    parameters to factor out to config file:
    - image size and ROI
    - folder for NB flatfielded data and speckle reconstruction
    - number of images per scan step
    - overlap of subimages (e.g. in my NB reconstruction i used 1.7 for 128 pix subimage size)

    TODO Double check the approach against Nazaret's code. At this moment this version applies the overlap,
    then does all the calculations on every chunk including the overlapping portions. Then the apod filter is
    applied to the overlaps to put the image back together again. Need to make sure this is correct.
    """
    logger = logging.getLogger(__name__)
    logger.setLevel("DEBUG")

    # Load reconstruction config parameters
    cfg = cfp.ConfigParser()
    cfg.read("vtf-calibration-config.cfg")
    config = cfg["nb-recon"]

    asdf_file = asdf.open(data_tree, mode="rw")
    if asdf_file["mode"] == "narrowband" and not bb_tree:
        raise ValueError(
            "Location of broadband data structure must be provided for reconstruction of narrowband data."
        )
    if asdf_file["mode"] != "narrowband":
        raise ValueError(
            f'Unrecognised data mode in asdf tree: {asdf_file["mode"]} '
            '- "mode" keyword should be either "broadband" or "narrowband".'
        )
    bb_tree = asdf.open(bb_tree, mode="rw")

    outdir = asdf_file["support"]["data_dir"]
    # Dark-correct speckle calibration files
    # I assume I need this but I'm not sure if I need to do flats as well
    if config.get("speckle_dir") or config.get("speckle_file"):
        logger.info("Using alternative speckle output file location, as specified in user config.")
        alt_speckle = True
    elif "speckle" not in list(bb_tree["support"].keys()):
        logger.warning("Speckle output file not found. Falling back to pre-calculated file.")
        alt_speckle = True
    else:
        alt_speckle = False

    if alt_speckle:
        speckdir = (
            Path(config.get("speckle_dir") or bb_tree["raw"]["data_dir"]).expanduser().absolute()
        )
        speckfname = config.get("speckle_file") or "speckle-output.FITS"
        speckfile = speckdir / speckfname
        logger.debug(f"Speckle output file: {speckfile}")
        headers = np.array(heads([speckfile]))
        bb_tree["support"]["speckle"] = refs(speckfile, headers, 1)
        bb_tree.update()

    # polstate = get_polarisation_state()

    # These also obviously need changing to work for all modstates
    modstate = "modstate0"
    nb = DFAC(
        asdf_file["support"][f"corrected flat-corrected data {modstate}"], loader=Loader
    ).array
    bb = np.flip(
        DFAC(bb_tree["support"][f"corrected flat-corrected data {modstate}"], loader=Loader,).array,
        axis=2,
    )
    # flat = DFAC(asdf_file["support"]["corrected wl_shifted flats"], loader=Loader).array
    flat = DFAC(
        asdf_file["support"][f"corrected dark-corrected flats {modstate}"], loader=Loader,
    ).array
    # This likely won't be where the speckle files end up
    logger.debug(bb_tree["support"].keys())
    speckled_bb = np.fliplr(DFAC(bb_tree["support"]["speckle"], loader=Loader).array)

    # Only reconstruct a single frame of the narrowband if specified in config (by index)
    if config.get("recon_step"):
        bb = bb[int(config["recon_step"])]
        bb = bb.reshape(1, *bb.shape)
        nb = nb[int(config["recon_step"])]
        nb = nb.reshape(1, *nb.shape)
        flat = flat[int(config["recon_step"])]
        flat = flat.reshape(1, *flat.shape)

    logger.debug(f"Narrowband images array shape: {nb.shape}")
    logger.debug(f"Speckle images array shape: {speckled_bb.shape}")
    for arr in [nb, bb, flat, speckled_bb]:
        arr[np.isnan(arr)] = np.nanmin(arr)
    plotframes(
        asdf_file,
        [
            (bb, "Flat-corrected broadband data"),
            (f"corrected flat-corrected data {modstate}", "Flat-corrected narrowband data",),
            (f"corrected dark-corrected flats {modstate}", "Dark-corrected flat"),
            (speckled_bb, "Speckle-reconstructed broadband data"),
        ],
        "reconstruction-temp0",
        color=["magma", None, None, "magma"],
    )

    sub = int(config["subimage"])
    chunksize = (sub, sub)
    logger.debug(f"Config specifies subimage size {sub}; setting chunksize={chunksize}")
    dx = int(config.get("overlap")) if config.get("overlap") else 8
    overlap = {0: 0, 1: dx, 2: dx}

    nb = da.rechunk(nb, (1, *chunksize))
    bb = da.rechunk(bb, (1, *chunksize))
    flat = da.rechunk(flat, (1, *chunksize))
    speckled_bb = da.rechunk(speckled_bb, chunksize)
    fftspeck_bb = speckled_bb.map_overlap(fft_chunks, depth=(overlap[1], overlap[2]), trim=False)
    fftspeck_bb = da.stack([fftspeck_bb] * len(nb))

    fft_bb = bb.map_overlap(fft_chunks, depth=overlap, trim=False)
    fft_nb = nb.map_overlap(fft_chunks, depth=overlap, trim=False)
    fft_flat = flat.map_overlap(fft_chunks, depth=overlap, trim=False)

    conjugate = fft_bb.conj()
    bfull = (fft_bb * conjugate).real
    nfull = fft_nb * conjugate
    ffull = fft_flat * conjugate

    nblocks = np.product(bfull.numblocks[1:])
    bsum = da.array(
        [
            bfull.blocks[:, i, j]
            for i in range(bfull.numblocks[1])
            for j in range(bfull.numblocks[2])
        ]
    ).sum(axis=0)
    nsum = da.array(
        [
            nfull.blocks[:, i, j]
            for i in range(nfull.numblocks[1])
            for j in range(nfull.numblocks[2])
        ]
    ).sum(axis=0)
    fsum = da.array(
        [
            ffull.blocks[:, i, j]
            for i in range(ffull.numblocks[1])
            for j in range(ffull.numblocks[2])
        ]
    ).sum(axis=0)

    nblocks = fft_bb.numblocks
    bsum = da.block([[bsum] * nblocks[1]] * nblocks[2])
    nsum = da.block([[nsum] * nblocks[1]] * nblocks[2])
    fsum = da.block([[fsum] * nblocks[1]] * nblocks[2])
    recon_noise = da.map_blocks(calc_noise, fft_bb, fsum, bsum)
    recon_im = da.map_blocks(calc_im, fftspeck_bb, nsum, bsum)
    data_power = recon_im.map_blocks(calc_power)

    optimum_filter = da.map_blocks(calc_optfilter, recon_noise, data_power)

    recon_nb = da.map_blocks(reconstruct_image, recon_im, optimum_filter, dx)
    recon_nb[recon_nb < 0] = np.nan
    nblocks = np.product(recon_nb.numblocks[1:])
    xy = np.array(recon_nb.shape)
    fullrecon = np.empty((nblocks, *xy))
    z = 0
    for i in range(recon_nb.numblocks[1]):
        for j in range(recon_nb.numblocks[2]):
            try:
                ix0, jx0 = (i * sub) + dx, (j * sub) + dx
                ix1, jx1 = ix0 + sub + (dx * 2), jx0 + sub + (dx * 2)
                block = recon_nb.blocks[:, i, j].real
                fullrecon[z, :, ix0:ix1, jx0:jx1] = block
            except ValueError as e:
                print(i, j, ix0, jx0, ix1, jx1, e)
            z += 1

    # TODO Exact values here need more work because they don't reliably place the image in the centre of the array
    trim = 2 * (dx - 2) * np.array(recon_nb.numblocks[1:])
    recon_nb = fullrecon[:, :, 2 * dx : -trim[0], 2 * dx : -trim[1]].sum(axis=0)
    data_fnames = [
        f.fileuri for f in asdf_file["support"][f"corrected flat-corrected data {modstate}"]
    ]
    old_headers = heads(data_fnames)
    recon_fnames = []
    for wl, frame in enumerate(recon_nb):
        fname = Path(outdir) / modstate / f"reconstructed_data_l{wl:02}a0.FITS"
        head = old_headers[wl]
        new_head = fits.Header(
            {kw: head[kw] for kw in ["VTF__002", "VTF__021", "VTF__014", "DKIST003"]}
        )
        fits.writeto(fname, frame.real, new_head, overwrite=True)
        recon_fnames.append(fname)
    asdf_file["support"][f"reconstructed data {modstate}"] = refs(
        recon_fnames, np.array(heads(recon_fnames)), len(recon_fnames)
    )
    try:
        img = recon_nb[TEST_WL_IDX].real
    except IndexError:
        img = recon_nb.real
    logger.debug(f"{img.min()}, {img.mean()}, {img.max()}, {img.std()}")

    logger.debug(f"{nb.shape}, {recon_nb.shape}")

    plotframes(
        asdf_file,
        [
            (f"corrected flat-corrected data {modstate}", "Flat-corrected data"),
            (f"reconstructed data {modstate}", "Reconstructed data"),
        ],
        "reconstructed-narrowband",
    )

    asdf_file.update()
